#/src/views/TeamView.py
from flask import request, g, Blueprint, json, Response
from ..shared.Authentication import Auth
from ..models.TeamModel import TeamModel, TeamSchema

team_api = Blueprint('team_api', __name__)
team_schema = TeamSchema()


@team_api.route('/', methods=['POST'])
@Auth.auth_required
def create():
  """
  Create Team Function
  """
  req_data = request.get_json()
  req_data['owner_id'] = g.user.get('id')
  data, error = team_schema.load(req_data)
  if error:
    return custom_response(error, 400)
  team = TeamModel(data)
  team.save()
  data = team_schema.dump(team).data
  return custom_response(data, 201)

@team_api.route('/', methods=['GET'])
def get_all():
  """
  Get All Teams
  """
  teams = TeamModel.get_all_teams()
  data = team_schema.dump(teams, many=True).data
  return custom_response(data, 200)

@team_api.route('/<int:team_id>', methods=['GET'])
def get_one(team_id):
  """
  Get A Team
  """
  team = TeamModel.get_one_team(team_id)
  if not team:
    return custom_response({'error': 'team not found'}, 404)
  data = team_schema.dump(team).data
  return custom_response(data, 200)

@team_api.route('/<int:team_id>', methods=['PUT'])
@Auth.auth_required
def update(team_id):
  """
  Update A Team
  """
  req_data = request.get_json()
  team = TeamModel.get_one_team(team_id)
  if not team:
    return custom_response({'error': 'team not found'}, 404)
  data = team_schema.dump(team).data
  if data.get('owner_id') != g.user.get('id'):
    return custom_response({'error': 'permission denied'}, 400)
  
  data, error = team_schema.load(req_data, partial=True)
  if error:
    return custom_response(error, 400)
  team.update(data)
  
  data = team_schema.dump(team).data
  return custom_response(data, 200)

@team_api.route('/<int:team_id>', methods=['DELETE'])
@Auth.auth_required
def delete(team_id):
  """
  Delete A Team
  """
  team = TeamModel.get_one_team(team_id)
  if not team:
    return custom_response({'error': 'team not found'}, 404)
  data = team_schema.dump(team).data
  if data.get('owner_id') != g.user.get('id'):
    return custom_response({'error': 'permission denied'}, 400)

  team.delete()
  return custom_response({'message': 'deleted'}, 204)
  

def custom_response(res, status_code):
  """
  Custom Response Function
  """
  return Response(
    mimetype="application/json",
    response=json.dumps(res),
    status=status_code
  )

